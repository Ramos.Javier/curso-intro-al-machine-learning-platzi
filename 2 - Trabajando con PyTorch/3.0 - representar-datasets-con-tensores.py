from typing import MutableMapping
import torch
import numpy as np
import pandas as pd


nunpyArray = np.random.randn(2,2)
# Pasamos de un array a tensor.
from_numpy = torch.from_numpy(nunpyArray)

# Mosttramos el tensor
print(from_numpy)

# Para obtener el promedio del tensor
print( "El primedio es : {}.".format(torch.mean(from_numpy)) )

print( "El primedio (col) es : {}.".format(torch.mean(from_numpy, dim=0)) ) # Devuelve : [ suma(col0) / 2, suma(col1) / 2 ]
print( "El primedio (fil) es : {}.".format(torch.mean(from_numpy, dim=1)) ) # Devuelve : [ suma(fil0) / 2, suma(fil1) / 2 ]

print ("Desviacion estandar (fil) es: {}.".format(torch.std(from_numpy, dim=1)) )

# Para guardar el tensor en un archivo.
torch.save(from_numpy, 'tensor.t')

# Para cargar el tensor.t a una variable.
load = torch.load('tensor.t')
print(load)


# Lo que hacemos con la libreria 'pandas' es leer el archivo.csv desde una url.
url = "fifa-data.csv"
dataframe = pd.read_csv(url)
print(dataframe)

# dropna : elimina las filas cuando cualquier celda sea null o este vacia
subset = dataframe[['Overall','Age', 'International Reputation', 'Weak Foot', 'Skill Moves']].dropna(axis=0, how='any')
print(subset)

columns = subset.columns[1:]

players = torch.tensor(subset.values).float()
print( "* Tamaño {}".format(players.shape) )
print( "* Tipo {}".format(players.type()) )

data = players[:, 1:]
print(data)
print( "* Tamaño {}".format(data.shape) )

# tomamos solo la primera columna (Overall)
target = players[:, 0]
print(target)
print( "* Tamaño {}".format(target.shape) )

mean = torch.mean(data, dim=0)
print( "Promedio {}".format(mean) )

std = torch.std(data, dim=0)
print("Desvicacion {}".format(std))

norm = (data - mean) / torch.sqrt(std)
print(norm)

#ge significa mayor o igual
good = data[torch.ge(target, 85)]
print(good)

# gt mayor que 70 ,lt menor que 85
avegage = data[torch.gt(target, 70) & torch.lt(target, 85)]
print(avegage)

notSoGood = data[torch.le(target, 70)]
print(notSoGood)

goodMean = torch.mean(good, dim=0)
averageMean = torch.mean(avegage, dim=0)
notSoGoodMean = torch.mean(notSoGood, dim=0)
print(goodMean)
print(averageMean)
print(notSoGoodMean)

for i, args in enumerate(zip(columns, goodMean, averageMean, notSoGoodMean)):
    print("{:25} {:6.2f} {:6.2f} {:6.2f}".format(*args))
    # {:25} texto de tamaño 25, {:6.2f} numero flotante con 6 digitos y 2 decimales