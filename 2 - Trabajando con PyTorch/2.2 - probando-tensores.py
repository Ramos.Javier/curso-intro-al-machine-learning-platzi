import torch
import numpy as np

torch.__version__

# Crea un tensor con numeros 1s de dimension 2x2
tensor_a = torch.ones(2,2)
tensor_a

# *************************************************************************************************************************
# NOTA IMPORTANTE : notar que la palabra Tesor esta con 'T' mayuscula, eso indica que los valores del tensor, son aleatorios.
# *************************************************************************************************************************

# Crea un tensor con numeros aleatorios
tensor_b = torch.Tensor(2,2)
print(tensor_b)

# Modifica o uniforma los valores del tensor, en este caso con un rango de 0 a 1.
tensor_b.uniform_(0, 1)

# En este caso crea un tensor de 2x2, de valores de 0 a 1 y random.
tensor_c = torch.rand(2, 2)
tensor_c

# Estoy creando un nuevo tensor, sumando tensor_b y tensor_c
# Nota: tambien tengo resta, multiplicacion, etc.
result = tensor_b + tensor_c
result

# Podemos redimensionar los tensores, por ej. a 1, 4 o un 4, 1
result.shape
reshaped = result.view(1, 4) # esto devolveria un arreglo de 4 columnas
reshaped
# Si lo pensamos 4, 1, tendremos 4 filas y una columna.

# *************************************************************************************************************************
# Ahora vamos a crear tensores con valores nuestros, ver la 't' minuscula.
# *************************************************************************************************************************

points = torch.tensor([[1.0, 2.0], [3.0, 4.0]])
print(points)

# Si queremos un elemento solo.
points[0][1] # devolveria 2.0

# Si queremos modificar.
points[0][1] = 2.5
points[0][1] # ahora devolveria 2.5

# Para saber de que forma se esta almacenando
points.storage()

# Para saber como paso de un numero al siguiente de cuerdo a las dimensiones.
points.stride() # esto devuelve (2,1) 2 columnas y 0-1 filas.

# Podemos hacer una transpuesta.
p_t = points.t()
p_t, p_t.stride() # vemos que ahora tenemos los vectores transpuesto y la dimension tambien cambia (1,2).

# Podemos agregar dimensiones
tensor_x = torch.tensor( [1,2,3,4] )
# Antes
tensor_x
# Despues
torch.unsqueeze(tensor_x, 0) # agregamos una dimension adicional con 0 sobre la misma fila

# Ahora la forma en que se agrego la dimension fue vertical.
torch.unsqueeze(tensor_x, 1) 
# esto nos va a servir cuando estemos manipulando imagenes.


# Tenemos tambien una interaccion con nunpy (nos sirve para el procesamiento de numeros)
# y trabajar entre otras cosas con machine learning.
nunpyArray = np.random.randn(2,2)
# Entonces podemos pasar de nunpy a tensor y viceversa.

# Creamos un tensor nuevo a partir de ese arreglo.
torch.from_numpy(nunpyArray)
