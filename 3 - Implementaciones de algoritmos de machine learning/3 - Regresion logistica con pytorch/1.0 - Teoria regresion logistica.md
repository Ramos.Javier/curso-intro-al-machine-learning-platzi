
# *******************************************************************************************************************************************
# Regresion logistica
# *******************************************************************************************************************************************


Volviendo al problema de los tacos, contrastamos el precio de un taco (regresion lineal) y si deberia comer tacos o no. En este problema en
particular la decision es si o no. Esto es un ploblema de clasificacion. En este caso tiene una categoria binaria (si como tacos o no como 
tacos). En base a la entrada, el resultado puede ser una probabilidad. Ej 0.8 que si deberia comer tacos o 0.65 no deberia comer tacos.

Ej. problemas de clasificacion

* Para resolver algunos problemas, es necesario predecir probabilidades.
* En los correos : es spam o no es spam.
* Examenes : aprobado y reprobado.

* Definicion : Son un mecanismo eficiente para calcular probabilidades. El resultado puede utilizarse tal cual o convertirlo a una categoria
binaria (para clasificar).


# Garantizando resultado [0,1] entre 0 y 1

Para una clasificacion binaria, nos apoyamos en una funcion matematica llamada 'sigmoide'. Si en caso la clasificacion tuviera mas parametros, 
hariamos uso de la funcion 'softmax'.


# Funcion sigmoid

[VER_img:1.1-funcion-sigmoid]

Podemos ver que en el grafico, el valor va entre 0 y 1. A la funcion lineal se le va a agregar la funcion sigmoide que nos va a dar de salida
una probabilidad y con esto resolvemos el problema de pasar de una regresion lineal (dominio continuo) a una regresion logistica que esta en 
el dominio discreto. Por este cambio la media de los cuadrados de los errores MSE, no va a ser la mejor forma de calcular el loss (perdida).
Entonces el loss function tambien tiene que cambiar.


# Funcion loss

La aproximacion intuitiva es 'castigar' cuando el valor es 0 y la prediccion resulta en 1 (o viceversa). Si yo espero que algo fuera 0 y me 
da un numero alto, tambien deberia 'catigarlo'.

[VER_img:1.2-loss]

Cuando lo que quiero es que mi valor valga 1, es la linea verde y cuando lo que quiero es que mi valor valga 0 es la azul. Entonces en la 
verde, si el valor es cercana a 0 el castigo es grande. Por otro lado en el azul si el valor es 1 el castigo es grande.

[VER_img:1.3-binary-cross-entropy-o-log-loss]

El logaritmo permite esto perfectamente. Entonces la funcion de perdida va a incluir logaritmos. Como lo que quiero es minimizar, le ponemos
el simbolo menos.
Nuestro algoritmo en pytorch va a tener el nombre de 'BCE'.

Con esto ya podemos modelar la funcion 'forward pass' como 'loss function'.

* Para problemas de probabilidad, utilizamos una regresion logistica.
* Para calcular el error (loss), nos basamos en la entropia pero el gradiente sigue siendo util.


# *******************************************************************************************************************************************