import torch
import torch.nn as nn
import numpy as np
from sklearn import datasets
import matplotlib.pyplot as plt

n = 100
h = n//2
dimen = 2

data = np.random.randn(n, dimen)*3

#print(data)
#plt.scatter(data[:,0], data[:,1])


data[:h, :] =  data[:h, :] - 3*np.ones((h, dimen))
data[h:, :] =  data[h:, :] + 3*np.ones((h, dimen))
'''
colors = ['blue', 'red']
color = np.array([colors[0]]*h + [colors[1]]*h).reshape(n)

plt.scatter(data[:,0], data[:,1], c=color)
plt.show()
'''

target = np.array([0]*h + [1]*h).reshape(n, 1)

x = torch.from_numpy(data).float().requires_grad_(True)

y = torch.from_numpy(data).float()

x.shape, y.shape

model = nn.Sequential(
    nn.Linear(2,1),
    nn.Sigmoid()
)

loss_function = nn.BCELoss()

optimizer = torch.optim.SGD(model.parameters(), lr=0.01)

losses = []

iterations = 1000

for i in range(iterations):
    result = model(x)

    loss = loss_function(result, y)
    losses.append(loss.data)

    optimizer.zero_grad()
    loss.backward()

    optimizer.step()


plt.plot(range(iterations), losses)

#plt.show()