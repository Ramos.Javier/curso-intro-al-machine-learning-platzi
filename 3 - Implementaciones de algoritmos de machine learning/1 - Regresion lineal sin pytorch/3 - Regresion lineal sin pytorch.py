
# Ref. Teoria y codigo : https://www.youtube.com/watch?v=Sv5IJ1AjjSU
# Teoria Coeficiente de determinacion o R cuadrado : https://economipedia.com/definiciones/r-cuadrado-coeficiente-determinacion.html

import numpy as np
import matplotlib.pyplot as plt

# Datos:
x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
y = [1, 2, 2, 4, 5, 4, 6, 4, 6, 7, 9, 10, 11, 12, 10]

n = len(x)

# Convertimos nuestra lista en vector/arreglo.
x = np.array(x)
y = np.array(y)

sumx = sum(x)
sumy = sum(y)
sumx2 = sum(x*x)
sumy2 = sum(y*y)
sumxy = sum(x*y)
promx = sumx / n
promy = sumy / n

m = (sumx*sumy - n*sumxy) / ( sumx**2 - n*sumx2)
b = promy - m*promx

print("m : {}".format(m))
print("b : {}".format(b))

# ******************************************************************************************************************
# NOTA : Para determinar que tan bueno es el ajuste, usamos algo que se conoce como el coeficiente de determinacion.
# ******************************************************************************************************************
sigmax = np.sqrt( sumx2/n - promx**2 )
sigmay = np.sqrt( sumy2/n - promy**2 )
sigmaxy = sumxy/n - promx*promy
R2 = ( sigmaxy/(sigmax*sigmay) )**2
# ******************************************************************************************************************

plt.plot( x, y,'o', label='Datos' )
plt.plot( x, m*x+b, label='Ajuste' )
plt.xlabel('x')
plt.ylabel('y')
plt.title('Mi primera regresion lineal')
plt.grid()
plt.legend()
plt.show()
