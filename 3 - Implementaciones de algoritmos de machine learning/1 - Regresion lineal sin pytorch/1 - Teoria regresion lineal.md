
# *******************************************************************************************************************************************


# Regresion lineal

La idea de esto es determinar la ecuacion de la recta que mejor se ajusta sobre los valores (datasets). En nuestro caso es el set de puntos 
(x,y).

Recordemos que una recta esta determinada por y = m*x + b, donde:

b = el punto de origen sobre el eje y.
m = la pendiente

[VER-2-Formulas]

Luego para ver que tan bueno es nuestro ajuste, existen muchos parametros estadisticos, pero el mas cumun es el siguiente.

# Coeficiente de determinación (R cuadrado)

El coeficiente de determinación es la proporción de la varianza total de la variable explicada por la regresión. El coeficiente de determinación, también llamado R cuadrado, refleja la bondad del ajuste de un modelo a la variable que pretender explicar. 

Es importante saber que el resultado del coeficiente de determinación oscila entre 0 y 1. Cuanto más cerca de 1 se sitúe su valor, mayor será el ajuste del modelo a la variable que estamos intentando explicar. De forma inversa, cuanto más cerca de cero, menos ajustado estará el modelo y, por tanto, menos fiable será.

* Nota : Obtenido R, lo elevamos al cuadrado.

# *******************************************************************************************************************************************