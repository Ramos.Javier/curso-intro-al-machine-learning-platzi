from typing import Sequence
import torch
import torch.nn as nn
import torch.optim as optim

from torchvision import utils
from torchvision import datasets
from torchvision import transforms

from PIL import Image
import urllib.request as request
from matplotlib import pyplot as plt


data_path = 'data/'

full_data_set = datasets.CIFAR10(data_path, train=True, download=True,
                                transform=transforms.Compose([
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.4915, 0.4823, 0.4468),
                                                        (0.2470, 0.2435, 0.2616))
                                ]))

full_validation_set = datasets.CIFAR10(data_path, train=False, download=True,
                                transform=transforms.Compose([
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.4915, 0.4823, 0.4468),
                                                        (0.2470, 0.2435, 0.2616))
                                ]))
size = 4
dataloader = torch.utils.data.DataLoader(full_data_set, batch_size=size, shuffle=True, num_workers=2)

len(full_validation_set)
len(full_data_set)

data_iterator = iter(dataloader)
imgs,labels = data_iterator.next()

classes = [ 'plane','car','bird','cat','deer','dog','frog','horse','ship','truck' ]

plt.imshow(utils.make_grid(imgs).permute(1, 2, 0))

for i in range(size):
    print(classes[labels[i]])

plt.show()

class_names = ['car', 'cat']

label_map = {1:0, 3:1}

'''
'''

data_set = [(img, label_map[label]) for img, label in full_data_set if label in [1,...]]
validation_set = [(img, label_map[label]) for img, label in full_validation_set if label in [1,...]]

img, label = data_set[20]
img.shape

plt.imshow(img.permute(1,2,0))
print(class_names[label])
plt.show()

model = nn.Sequential(
    nn.Linear( 3 * 32 * 32, 512),
    nn.Tanh(),
    nn.Linear(512, 2),
    nn.Softmax(dim=1)
)

loss_function = nn.NLLLoss()

plt.imshow(img.permute(1,2,0))
print(class_names[label])
plt.show()

out = model(img.view(-1).unsqueenze(0))
print( out )

print('->', class_names[label], 'model:', class_names[index])

# Parte 3 - Mejoras limitaciones y conclusines

train_loader = torch.utils.data.DataLoader(data_set, batch_size=64, shuffle=True)

model = nn.Sequential(
    nn.Linear( 3072, 1024 ),
    nn.Tanh(),
    nn.Linear( 1024, 512 ),
    nn.Tanh(),
    nn.Linear( 512, 128 ),
    nn.Tanh(),
    nn.Linear( 128, 2 )
)

loss_function = nn.CrossEntropyLoss()

optimizer = optim.SGD(model.parameters(), lr=0.01)

n_epochs = 100

for epoch in range(n_epochs):
    for img, label in train_loader:
        out = model(img.view(img.shape[0], -1))
        loss = loss_function(out, label)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print("Epoch: %d, Loss:%f" % (epoch, float(loss)))


correct = 0
total = 0

# validacion de que tan bien se desempeña el modelo.

train_loader = torch.utils.data.DataLoader(data_set, batch_size=64, shuffle=False)

with torch.no_grad():
    for imgs, labels in train_loader:
        outputs = model(imgs.view(imgs.shape[0], -1))
        _, predicted = torch.max(outputs, dim=1)
        total += labels.shape[0]

        correct += int((predicted == labels).sum())

print(" Accuracy: %f" % (correct/total))

sum([p.numel() for p in model.parameters() if p.requires_grad == True])



'''
def softmax(x):
    return torch.exp(x) / torch.exp(x).sum()

x = torch.tensor([1.0, 2.0, 3.0])

softmax(x).sum()

out = model(img.view(-1).unsqueenze(0))
print( out )

_, index = torch.max(out, dim=1)

print('->', class_names[label], 'model:', class_names[index])
'''