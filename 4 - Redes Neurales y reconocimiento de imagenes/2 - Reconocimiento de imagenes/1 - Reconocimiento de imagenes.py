import torch
import torch.nn as nn

from torchvision import utils
from torchvision import models
from torchvision import datasets
from torchvision import transforms

from PIL import Image
import urllib.request as request
from matplotlib import pyplot as plt

# Modelo pre-entrenado
inception = models.inception_v3(pretrained=True)

#url = 'https://i.pinimg.com/474x/d1/b5/8d/d1b58dc866f4f772fcccced686e82c09.jpg'
url = 'https://i.pinimg.com/originals/98/21/3a/98213a4cf2edc2e84750c7b83cf331d5.jpg'

image = request.urlretrieve(url, 'test.jpg')
img = Image.open(image[0])
print(img)

preprocess = transforms.Compose([
    transforms.Resize(128),
    transforms.CenterCrop(98),
    transforms.ToTensor(),
    transforms.Normalize(
        mean = [0.485, 0.456, 0.406],
        std = [0.229, 0.224, 0.225]
    )
])

img_t = preprocess(img)
batch = torch.unsqueeze(img_t, 0)
plt.imshow(img_t.permute(1, 2, 0))

inception.eval()

out = inception(batch)
print(out)

#request.urlretrieve('https://gist.github.com/ykro/acb00a36f737c12013f6e0f8a0d2cb61', 'labels.txt')

with open('imagenet_classes.txt') as f:
    labels = [ line.strip() for line in f.readlines() ]

_, index = torch.max(out, 1)

percentage = torch.nn.functional.softmax(out, dim=1)[0] * 100
print("EL TOP 1")
print( labels[index[0]], percentage[index[0]].item() )

print("LOS TOP 5")
_, indices = torch.sort(out, descending=True)
top_five = indices[0][:5]
for i in top_five:
    print( labels[i], percentage[i].item() )
