import torch
import torch.nn as nn

from torchvision import utils
from torchvision import models
from torchvision import datasets
from torchvision import transforms

from PIL import Image
import urllib.request as request
from matplotlib import pyplot as plt

data_path = "data/"

dataset = datasets.CIFAR10(data_path, train=True, download=True)

validation_set = datasets.CIFAR10(data_path, train=False, download=True)

classes = [ 'plane','car','bird','cat','deer','dog','frog','horse','ship','truck' ]
 
print(len(dataset))

img, label = dataset[50]
print(img, label, classes[label])

#plt.imshow(img)
#plt.show()

tensor_transform = transforms.ToTensor()

img_tensor = tensor_transform(img)

print(img, img_tensor, img_tensor.shape)

dataset = datasets.CIFAR10(data_path, train=True, download=True, transform=transforms.ToTensor())

img, label = dataset[50]
print(img)

print( img.min(), img.max() )

#plt.imshow(img.permute(1,2,0))
#plt.show()

imgs = torch.stack([img_tensor for img_tensor, _ in dataset], dim=3)

print( imgs.shape )

print(imgs.view(3, -1).mean(dim=1))

print(imgs.view(3, -1).std(dim=1))

transform = transforms.Compose(
    [
        transforms.ToTensor(),
        transforms.Normalize((0.4915, 0.4823, 0.4468), (0.2470, 0.2435, 0.2610))
    ])

dataset = datasets.CIFAR10(data_path, train=True, download=True, transform=transform)

img_transformed,label = dataset[125]

plt.imshow( img_transformed.permute(1,2,0) )
plt.show()
print(classes[label])