from sklearn.datasets import load_iris
from sklearn.ensemble import RandomForestClassifier

import pandas as pd
import numpy as np

np.random.seed(0)

iris = load_iris()

df = pd.DataFrame(iris.data, columns=iris.feature_names)
print( df.head() )

df['species'] = pd.Categorical.from_codes(iris.target, iris.target_names)
print( df.head() )

trainRandom = np.random.rand(len(df)) < 0.8

train = df[trainRandom]
test = df[~trainRandom]

print( 'Numero obs en el set de entrenamiento =',len(train) )
print( 'Numero obs en el set de prueba =',len(test) )

features = df.columns[:4]

print( features )
print( df.species )

y = pd.factorize(train['species'])[0]
print( y )

clf = RandomForestClassifier(n_jobs=2, random_state=0)
clf.fit(train[features], y)

print( clf.predict(test[features]) )

preds = iris.target_names[clf.predict(test[features])]
print( preds[0:15] )

print( test['species'].head() )

# Matriz de confusion.
print( pd.crosstab(test['species'], preds, rownames=['Actual Species'], colnames=['Predicted Species']) )

# Para saber cual de nuestros features toman mayor relevancia en mi algoritmo de aprendizaje.
print( list(zip(train[features], clf.feature_importances_)) )
