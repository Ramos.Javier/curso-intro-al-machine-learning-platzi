# Curso-Intro-al-Machine-Learning-Platzi

Curso de Introduccion al Machine Learning en Platzi.

## 1 - Conceptos básicos de Machine Learning

* 01 - Bienvenida Curso de Introducción a Machine Learning
* 02 - Introducción a la terminología de Machine Learning
* 03 - Terminología y regresión lineal
* 04 - Training - Los Entrenando y ajustando nuestro modelo

## 2 - Trabajando con Pytorch

* 01 - Introducción a Pytorch, trabajar con tensores y representar datasets con tensores
* 02 - Trabajando con tensores
* 03 - Representando datasets con tensores

## 3 - Implementaciones de algoritmos de Machine Learning

* 02 - Regresión logística
* 03 - Implementación de regresión logística en Pytorch

## 4 - Redes Neurales y reconocimiento de imágenes

* 02 - Neuronas y función de activación, Usando un modelo pre entrenado para reconocimiento de imágenes, Tr
* 03 - Usando un modelo pre entrenado para reconocimiento de imágenes
* 04 - Trabajando un dataset

## 5 - Reconocimiento de imágenes

* 01 - Construyendo un modelo, Implementando un clasificador totalmente conectado, Mejoras, limitaciones y
* 02 - Implementando un clasificador totalmente conectado
* 03 - Mejoras, limitaciones y conclusiones

## 6 - Collab con Scikit

* 01 - Aprende a usar Collab con scikit
* 02 - Demo con Scikit división de datos
* 03 - Demo con Scikit validación de datos

## 7 - Algoritmos más usados en Machine Learning

* 01 - Algoritmos supervisados en Machine Learning
* 02 - Los algoritmos más usados en Machine Learning
* 03 - Algoritmos no supervisados en Machine Learning

## 8 - Bonus Redes neuronales y herramientas

* 01 - Qué es lo que está detrás de una red neuronal
* 02 - Cómo funciona una red convolucional intuitivamente y porque son tan buenas con imágenes
* 03 - Redes generativas

## 9 - Cierre

* 01 - Conclusiones y siguientes pasos
